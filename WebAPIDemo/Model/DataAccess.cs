﻿using MongoDB.Driver;
using MongoWebApiDemo.DAL;
using WebAPIDemo.Model;
using System.Collections.Generic;

namespace WebAPIDemo.Model
{
    public class DataAccess
    {
        public static void SeedDatabase(MongoDBContext context)
        {
            //db has books already
            if (context.Books.Find(b => true).Count() != 0) return;

            List<Book> e=new List<Book>()
            
            {
                new Book{BookID = 0, Author = "Yeshwant KAnetkar", title = "Let us C++", Price = 18.99},
                new Book{BookID = 1, Author = "J.R.R. Tolkien", title = "Lord of the Rings", Price = 22.99},
                new Book{BookID = 2, Author = "Moby Dick", title = "Herman Mellvile", Price = 11.99},
                new Book{BookID= 3, Author = "Hamlet", title = "William Shakespear", Price = 14.99},
            };

            context.Books.InsertMany(e);
        }
    }
}
   
