﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using WebAPIDemo.Model;
using System.Collections.Immutable;
using System.Collections;


namespace MongoWebApiDemo.DAL
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database = null;
        public MongoDBContext()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            if (client != null)
                _database = client.GetDatabase("BookDemoDb");
        }

        public IMongoCollection<Book> Books
        {
            get
            {
                return _database.GetCollection<Book>("Books");
            }
        }
        public IEnumerable GetBooks()
        {
            return this.Books.Find(b => true).ToList();
        }

        public Book GetBook(int id)
        {
            var filter = Builders<Book>.Filter.Eq("BookID", id);
            return this.Books.Find(filter).FirstOrDefault();
        }

        public Book InsertBook(Book book)
        {
            this.Books.InsertOne(book);
            return book;
        }

        public void ReplaceBook(Book book)
        {
            var filter = Builders<Book>.Filter.Eq("BookID", book.BookID);
            this.Books.ReplaceOne(filter, book);
        }

        public void RemoveBook(int id)
        {
            var filter = Builders<Book>.Filter.Eq("BookID", id);
            this.Books.DeleteOne(filter);
        }

    }
}
    