﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPIDemo.Model
{
    public class Book
    {
        [BsonId]

        public int BookID { get; set; }

        public string title { get; set; }
        [Required]
        public string Author { get; set; }

        public double Price { get; set; }
    }
}
