﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MongoWebApiDemo.DAL;
using WebAPIDemo.Model;
using Microsoft.Extensions.Logging;

namespace WebAPIDemo.Controllers
{
    [Produces("application/json")]
    [Route("api/Books")]
    public class BooksController : Controller
    {
        //Test comment
        MongoDBContext _context;

        private readonly ILogger<BooksController> _logger;

        public BooksController(ILogger<BooksController> logger)
        {
            _logger = logger;
            _context = new MongoDBContext();
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index page says hello");
            return View();
        }

        // GET api/books
        [HttpGet]
        public IEnumerable Get()
        {
            return _context.GetBooks();

        }

        // GET api/books/5
        /// <summary>
        /// Deletes a specific TodoItem.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Book Get(int id)
        {
            return _context.GetBook(id);
        }

        /// <summary>
        /// create new book
        /// </summary>
        /// <remarks>
        ///     POST /Todo
        ///     {
        ///        "id": 1,
        ///        "name": "Item1",
        ///        "isComplete": true
        ///     }
        /// </remarks>
        /// <param name="book"></param>
        /// <returns>A newly-created Book</returns>
        /// <response code="201">Returns the newly-created Book</response>
        /// <response code="400">If the item is null</response>  
        // POST api/books
        [HttpPost]
        [ProducesResponseType(typeof(Book), 201)]
        [ProducesResponseType(typeof(Book), 400)]
        public void Post([FromBody]Book book)
        {
            _context.InsertBook(book);
        }

        // PUT api/books/5
        [HttpPut]
        public void Put([FromBody]Book book)
        {
            _context.ReplaceBook(book);
        }
    
        // DELETE api/books/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _context.RemoveBook(id);
        }
    }
}